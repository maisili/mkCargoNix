{
  description = "mkCargoNix";

  outputs = { self, lib }@fleiks:
  {
    strok = {
      spici = "lamdy";
    };

    datom = {
      pkgs, rust, buildRustCrate, defaultCrateOverrides,
      kreitOvyraidz, nightlyRust
    }@argz:

    {
      cargoNixPath,
      nightly ? false,
    }@yrgz:

    let
      lib = fleiks.lib.datom;

      defaultCrateOverrides = argz.defaultCrateOverrides
      // kreitOvyraidz;

      buildRustCrate = if nightly
      then argz.buildRustCrate.override { rustc = nightlyRust.rust; }
      else argz.buildRustCrate;

      cargoNix = import cargoNixPath {
        inherit lib pkgs buildRustCrate defaultCrateOverrides;
      };

    in cargoNix;

  };
}
